import haxe.ds.*;
//import Sure.sure;
// import Assert.*;
using StringTools;
using Slambda;
using stdlib.StringTools;
// using stdlib.StringTools; 
// import stdlib.Regex;
// import Helpers.Assert.*;
import Sure.sure as assert;
import Sure.sure as require;
import Sure.sure as ensure;
using Helpers.ERegTools;


// use System.Text.RegularExpressions
class ParsingUtils
{	
		static var command = "(?'command'[a-zA-Z]+($|\\s+))"; 
		static var subCommands = "(?'subs'(([a-zA-Z]+)($|\\s+))*)"; 
		static var config = "(?'configs'((-[A-Za-z0-9]+:[A-Za-z0-9]+)($|\\s+))*)"; 
		static var fullcmd = command + subCommands + config;
		//static var fullregex = "^(?'command'[a-zA-Z]+($|\\s+)){1,1}(?'subs'(([a-zA-Z]+)($|\s+))*)(?'configs'(-[A-Za-z0-9]+:[A-Za-z0-9]+($|\s+))*)$";
		static var oneConfig = ~/(-[A-Za-z0-9]+:[A-Za-z0-9]+)/;
		static var oneCmd = ~/[a-zA-Z]+/g;
	static function isValidCommand(cmd : String) : Bool
		{
			require( {
			cmd != null;
			cmd.length > 0; // not empty
			});
			var regex = new EReg(fullcmd,"");
			var res = regex.match(cmd);
			return res; 
		}
	public static function parseCommand(cmd_and_args : String) : ParsedCommand {
		// implement with simple text tools.
		var spaces = ~/[\s]+/g;
		var split = spaces.split(cmd_and_args);		
		var cmd = "", subs = [], args =  new StringMap<String>();
		// we have one command.
		if (split.length > 0)
			cmd = split[0];
			
		if (split.length > 1) // we have subs
			{
				var first_config_idx = 1000; 
				for (i in 1 ... split.length)
				{
					var value = split[i];
					// if (value.startsWith("-"))
					if (oneConfig.match(value)) // we have a config
					{
						if (first_config_idx == 1000) first_config_idx = i; // store only the first
						
						var arr2 = value.split(":");
						var k = arr2[0].ltrim('-'), val = arr2[1];
						args.set(k,val);
					}
					else {
						// we have a sub
						assert(i < first_config_idx);
						assert(oneCmd.match(value));
						subs.push(value);
						}
					
				}
			}
		
		return new ParsedCommand(cmd,subs,args);
	}
	public static function parseCommand_withGroups(cmd_and_args : String) : ParsedCommand {
			require(cmd_and_args.length > 0); // require: not empty
		
			var lower = cmd_and_args.toLowerCase();			
			var cmd = "";
			var subs:Array<String> = [];
			var configDict = new StringMap<String>();
						
			if (!isValidCommand(lower)) {
				var why = "validation failed.";
				var res = ParsedCommand.asError(why);
				return res;
			}
			else
			{
			// start parsing: gets the regex groups.
				var cmdregex = new EReg(fullcmd,"i");
				var matched_groups = cmdregex.groups(lower);
				
				trace(matched_groups);
				
				if (matched_groups.exists('command'))
					cmd = matched_groups.get('command').join(" ").trim();
				
				trace(cmd);
				
				if (matched_groups.exists('subs'))
					subs = matched_groups.get('subs');
					subs = subs.map.fn(_.trim());
					
				trace(subs);
				if (matched_groups.exists('configs')) 
				{
				var configsStr = matched_groups.get('configs')[0].trim();
				// trace(configsStr);
				
					if (configsStr.length > 0) 
					{
						var configs = configsStr.split(' ');
						trace (configs);
						for (cfg in configs) 
						{
							var pair = cfg.split(':');
							// trace(pair);
							assert(pair.length==2);
							var key = pair[0].ltrim('-'), val = pair[1];
							configDict.set(key,val);
						}
						trace (configDict);
					}
				}
				var res = new ParsedCommand(cmd, subs, configDict);
				trace (res);
				return res;
			}
	}

}
