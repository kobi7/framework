
interface IControllerSpec<Parts> {
	function beforeAction(cmd_and_args : ParsedCommand):Void;
	function customAction(cmd_and_args : ParsedCommand) : Response;
	function afterAction(cmd_and_args : ParsedCommand):Void ;
	
	function attachView(v1 : IView<Parts>):Void;
	function detachView(v1 : IView<Parts>):Void;
	
	var roles(default,null) : Array<IRole>;
	
	function notifyViews(note : String):Void;
	function sendErrToViews(err : Response):Void;
	function updateViews():Void;
	function updateViewPart(v1 : IView<Parts>, data : SimpleData<Parts>):Void;
	function getDataForPart(part : Parts) : SimpleData<Parts>;
	
	function doSaveState():Void;
}