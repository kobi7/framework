//import haxe.ds.*;

interface IView<Parts> {
	function setupUi():Void;
	function requestUpdate():Void;
	function start():Void;
	function doExit():Void;
	function startedSendingUpdates():Void;
	function finishedSendingUpdates():Void;
	function notify(note: String):Void;
	function redrawPart(data : SimpleData<Parts>):Void;
	function showErr(resp : Response):Void;
	function redrawAll(drawing_data : Map<Parts, SimpleData<Parts>>):Void; // TODO: maybe unneeded.

}
