//	An error enum, to be used in the Response struct
enum ErrorType {
	None;
	Uninitialized;
	Parse;
	Validation;
	IO;
	UnknownCommand;
	UnknownSubCommand;
	UnknownConfigKeys;
	BadValue;
	ValueNotInRange;
	}
	