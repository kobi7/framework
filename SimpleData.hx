class SimpleData<Parts> {
	var part(default,null) : Parts;
	var data(default,null) : Dynamic; 
	
	public function new(part:Parts, data:Dynamic) {
		this.part = part;
		this.data = data;
	 }		
	}
	
	// TODO: find out where/when data becomes string (what view gets).