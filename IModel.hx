interface IModel<Parts>{
	function makeSimplifiedData() : SimpleData<Parts>;
}