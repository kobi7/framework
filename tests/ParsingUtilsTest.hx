import haxe.ds.*;
import mohxa.Mohxa.*;
import Sure.sure as assert;
import ParsingUtils.parseCommand;
import Helpers.GeneralTools.*;
using Helpers.GeneralTools;


// use tests to see that I parsed the parts correctly. (a few possible commands-config combinations).
class ParsingUtilsTest extends mohxa.Mohxa {
		public function new() { 
		super();
		describe('just cmd', function(){
			var ex1 = ParsingUtils.parseCommand("exit"); // just cmd
			//log('command is: exit');
			it('should have assign command to field command.', function () {
				equal(ex1.command , "exit");
			});	
		});
		describe('cmd sub', function () {
		 	var ex2 = parseCommand("app about"); // cmd sub
			it('command should be app', function () {
				equal(ex2.command ,"app");
			});
			
			it('should have subcommands', function () {
				equal(true, ex2.subCommands.array_eq([ "about" ]));
			});
			
		});
			
			describe('cmd config', function () {
		 	var ex3 = parseCommand("init -start:quiet"); //  cmd config
			it('should have cmd: init', function () {
				equal(ex3.command,"init");			
			});
			it('should have a dictionary for configs', function () {
				var configs = ["start" =>"quiet"];
				equal(true, eq(ex3.arguments, configs));
			});
				
			});
		
		describe('cmd sub configs', function () {
			//  cmd sub1 sub2 sub3 configs
			it('should have all', function () {
					
				var ex4 = parseCommand("convert audio for sure -input:mp3 -output:wav -bitrate:224");
				equal (ex4.command , "convert");
				equal (true,array_eq(ex4.subCommands , [ "audio", "for", "sure" ]));
				var configs = ["input"=>"mp3","output"=>"wav", "bitrate"=>"224"];
				equal (true, eq(ex4.arguments , configs));
				});
			});
			
			
		}
}