import mohxa.Mohxa;

using Helpers;
using Lambda;

class ERegToolsTest extends Mohxa 
{
	public function new() 
	{ 
		super();
		describe('matches', function () {
			it('should return a correct list of matches', function () {
				var regex = ~/\d+/;
				var text = "hello agent 123, how are you 2day?";
				var numbers = regex.matches(text);
				equal(numbers.toString(), ["123","2"].toString());
			});
		});
		describe('named captures 1', function () {
			it('should match', function () {
				var reggie = ~/(?'name'([\w\.]+))\s+(?'email'(<[a-z0-9]+@some.domain.net>))/i;
				var text = "Mr. ho <mrho9@some.domain.net>";
				var groups = reggie.groups(text);
				trace(groups);
				equal(groups.exists('name'), true);
				equal(groups.exists('email'), true);
				equal(groups['name'].length,1);
				equal(groups['email'].length,1);
				
				equal(groups['name'][0], "Mr. ho");
				equal(groups['email'][0], "mrho9@some.domain.net");
				 
			});
		});
		describe('named captures 2', function () {
			it('should not match', function () {
				equal(1,2); // TODO
			});
		});
		
		
	}
	
}