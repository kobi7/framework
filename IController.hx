// api for view
interface IController<Parts> 
{
	function requestReload():Void;
	function requestPartialReload(part : Parts):Void;
	function load():Void;
	function performAction(cmd : String) : Response;
}