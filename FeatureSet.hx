// Options for controller's operation, mix and match with '|' as these are flags
@:enum abstract FeatureSet(Int) from Int to Int
	{
		var None = 0;
		var ReplayLog		= value(0);
		var UndoRedo		= value(1);
		var Profile			= value(2);
		var Persistence = value(3);

	static inline function value(index:Int) return 1 << index;
	inline public static function add(bits:Int, mask:Int):Int	return bits | mask;
	inline public static function remove (bits:Int, mask:Int):Int	return bits & ~mask;
	inline public static function contains (bits :Int, mask :Int):Bool return bits & mask != 0;
	inline function new(v:Int) this = v;
	inline function toInt():Int	return this;
}

