
class Response { 
	
	var why(default,null):String;
	var ok(default,null):Bool;
	var error(default,null):ErrorType; 
	
public function new(error:ErrorType, ok:Bool = false, but_why:String="") {
		this.ok = ok;
		this.error = error;
		this.why = but_why;
	}

		
	public static function okay() : Response 
	{
		var res = new Response(ErrorType.None, true);
		return res;
	}
}
