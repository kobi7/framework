import stdlib.Uuid;
import haxe.ds.StringMap;
using Lambda;

class ParsedCommand {
	
	public var command : String;
	public var subCommands : Array<String>;
	public var arguments : StringMap<String>;
	public var error: Response;
	public var id(default, null) : String; 
		

	public function new(cmd:String, ?sub:Array<String>,	?args:StringMap<String>)
	{
		error = Response.okay();
		id = Uuid.newUuid();
		
		this.command = cmd;
		subCommands = sub == null? [] : sub;
		arguments = args==null? new StringMap<String>() : args;
	}
	
	public function hasArgs() : Bool	return arguments.array().length > 0;
	public function hasSubCmds()	return subCommands.length > 0;
	public function isOnlyCmd() : Bool	return (!hasSubCmds() && !hasArgs());
		
	public static function asError(why: String) : ParsedCommand
	{
		var res = new ParsedCommand("");
		res.error= new Response(ErrorType.Validation, why);
		return res;	
	}
}