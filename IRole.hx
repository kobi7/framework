
interface IRole {
	public var tasked_with(default,never):String;
	
	 // check that the environment allows its operation, e.g a logger will check ability to write to disk.
	function checkFeasability():Void;
	
}