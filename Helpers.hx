import haxe.macro.Expr;
using haxe.macro.Tools;
// import Sure.sure as assert;
import haxe.io.*;
import haxe.std.*;
// using haxe.std;
import sys.FileSystem.*;
import haxe.ds.StringMap;

import sys.io.File.*;
using Lambda;
using StringTools;
using Slambda;

class Helpers {}
class NullTools 
{
	public static function or<T>(arg :Null<T> , other:T ) : T
	{
		if (arg != null)
			return arg;
		else
			return other;
	}
	
}

class GeneralTools {
	public static function toArray<T> (it:Iterator<T>) : Array<T>
	{
		var result:Array<T> = [];
		while(it.hasNext())
			result.push(it.next());
	
		return result;
	} 				
	public static function all (args  :Array<Bool>) : Bool
	{
		var result:Bool= true;
		for (item in args)
			if (item == false)
				return false;
	
		return result;
	}
	public static function array_eq<A> (arr1 :Array<A> , arr2:Array<A> ) : Bool
	{
		if (arr1.length != arr2.length) return false;
		for (i in 0 ... arr1.length)
			if (arr1[i] != arr2[i])
				return false;
				
		return true;
	}
	
	public static function eq<A,B> (dict1 :Map<A,B> , dict2:Map<A,B> ) : Bool
	{
		var result = true;
		var len1 = GeneralTools.toArray(dict1.keys()).length;
		var len2 = GeneralTools.toArray(dict2.keys()).length;
		trace(len1,len2);
		if (len1 != len2) return false;
		
		for (k in dict1.keys())
		{
			
			if (!dict2.exists(k))	return false;
			else
			{
				// key ok, check value.
				var val1 = dict1.get(k);
				var val2 = dict2.get(k);
				var res;
				if(Std.is(val1,Array) && Std.is(val2,Array))
				{	
					res = array_eq(cast (val1, Array<Dynamic>) , cast(val2, Array<Dynamic>));
				}
				else
				{
					res = val1 == val2; 
				}
					
				if (!res)	return false;
			}
		}
		return result;
	}
	
	
	public static function cmp(a,b) : Int {
		if (a < b) return -1;
		else if (a > b) return 1;
		return 0;
	}
	
	 public static function histogram(list:Array<String>) : Map<String,Int> {
		 var dict = new Map<String, Int>();
		 for (item in list)
		 {
			 if (!dict.exists(item))
			 {
				 dict.set(item,1);
			 } 
			 else
			 {
				 var count = dict.get(item);
				 count+=1;
				 dict.set(item,count);
			 }
		 }
		 return dict;
	 }
	 
	 public static function flatten<A>(x:Array<Array<A>>) : Array<A> 
	 {
		 var res = [];
		 for (ls in x)
		 {
			 for (item in ls)
			 	res.push(item);
		 }
		 return res;
	 }
	public static function values<K,V>(dict: Map<K,V>) : Array<V> 
	{
		var res = [];
		for (k in dict.keys())
		{
			res.push(dict.get(k));
		}
		return res;
		
	} 
	
	public static function pairs_from_dict<K,V>(dict: Map<K,V>) : Array<Pair<K,V>>
	{
		var res = [];
		for (k in dict.keys())
		{
			res.push({ first:k, second: dict.get(k)});			
		}
		return res;
	}
		
}



class DirTools 
{
	public static function subfiles(dir:String):Array<Path> {
	var list = [];
	var files = readDirectory(dir);
	for (f in files)
		{
			var file = dir+"/"+f;
			if (!isDirectory(file))
				list.push(new Path(file));
		}	 
		return list;
	}
	
	public static function list_files_sub (path :Path ) : Array<Path>
	{
		var result:Array<Path> = [];
		//determine if argument is file or directory.
		var readpath = if (isDirectory(path.toString())) path.toString() else path.dir;

		var files = readDirectory(readpath);
		for (f in files)
		{
			var	full = new Path(readpath+"/"+f);
			var	subfiles = [];
			var fullstr = full.toString();
			if (!isDirectory(fullstr)) 
			{ 
				var file = full;			 
				result.push(file); 
			}
			else
			{
				var subfiles = list_files_sub(full); //dir
				result = result.concat(subfiles); // add subfiles. (flattened)
			}
		}
	
		return result;
	}
		
}

class EsotericTools {
	
	
	public static function find_needs (the_files:Array<Path>)
	 {
		 // trace("finding needs of each vocab");
		var dict = new StringMap<Array<String>>();
		for (file in the_files)
		{
			var res = get_usings(file.toString());
			dict.set(res.vocab, res.needs);
		}
		return dict;
	 }
	 
	public static function top_needs(dict, n:UInt) {
		 		var vals = GeneralTools.values(dict);
		var flat = GeneralTools.flatten(vals); 
		var hist = GeneralTools.histogram(flat);
		
		var pairs = GeneralTools.pairs_from_dict(hist);
		pairs.sort(function(a:Pair<String,Int>,b:Pair<String,Int>):Int{
			return GeneralTools.cmp(a.second, b.second);			
		});
	}
		
	public static function get_usings (file :String ) 
	{
		// read contents of file.
		var text = getContent(file);
		// find occurence of IN: until \n , this is the name of the vocab.
		var vocabNameRegex = ~/IN:\s+(\w+).*\n/ ;
		// find occurences of USE: until \n and USING: until ; then, concat together.
		var use_regex = ~/USE:\s+(\w+).*\n/;
		var using_regex = ~/USING:\s(([\w.]+)\s)+;/;
		
		var vocab="";
		if(vocabNameRegex.match(text))
			vocab = vocabNameRegex.matched(1);
			
		var use = [];
		if (use_regex.match(text))
			use.push(use_regex.matched(1));
			
		if (using_regex.match(text))
			{
				var	usingstr = using_regex.matched(0).replace("\n"," ");
				var inner = ~/USING:\s([\w.\s]+)/; 
				if (inner.match(usingstr))
				{
					var res = inner.matched(1).trim();
					for (vocab in res.split(" "))
					{
						use.push(vocab);
					}
				}
				
				// use.push(usingstr);
				
				
				}
			
		// return the pair as an anonymous structure.
		return { vocab:vocab, needs:use };
	}
 
 	public static function list_factor_files (path :Path, ?pred: Path -> Bool) : Array<Path>
	 {
		var all = DirTools.list_files_sub(path);
		if (pred== null) return all;
		
		var filtered = all.filter(pred); // TODO: FIGURE OUT!
		return filtered;
		
	 }
	public static function dict_to_dot_digraph(dict: StringMap<Array<String>>) :String 
	{
		var output:String = "";
		
		function start_output() {
			output += "digraph {\n";
		}
		function finish() {
			output += "}";
		}
		
		start_output();
		for (k in dict.keys())
		{
			var list = dict.get(k);
			for (v in list){
				output += '$k -> $v\n';
			}
		}
		finish();
		return output;
	}
		
	
}
class ERegTools
 {
	// extend ereg. to have matches....
	// and named captures.
	
	static function getNames(regex:EReg, text:String) : StringMap<String>
	{
		
		var dict = new StringMap<String>();
		// var more = ~/(\(\?'[\w]+'.*?\)\))/;
		var more = ~/(\(\?'[\w]+'.*?\)\)({\d,\d}|[)*]+)?)/;
		var named = ~/(\?'[\w]+')/;
		//var textRegex = "^(?'command'[a-zA-Z]+($|\\s+)){1,1}(?'subs'(([a-zA-Z0-9]+)($|\\s+))*)(?'configs'(-[A-Za-z0-9]+:[A-Za-z0-9]+($|\\s+))*)$";
		var list = ERegTools.matches(more, text);
		// trace(list);
		for (item in list)
		{
			// trace(item); 
			var name = one_match(named, item).part;
			// trace(name);
			
			
			var remaining:String = named.replace(item,"");
			// trace(remaining);
			var actualName = one_match(~/[\w]+/, name).part;
			// trace(actualName);
			dict.set(actualName,remaining);
		}
		trace(dict);
		return dict;
	}
	
	public static function groups (regex:EReg, text:String) : Map<String,Array<String>>
	{
		var resultsDict = new StringMap<Array<String>>();
		var nameDict = getNames(regex, text);
		 trace(nameDict);
		for (k in nameDict.keys())
		{
			// trace(k);
			var val = nameDict.get(k).trim();
			// trace(val);
			var reg = new EReg(val,"g");
			var matches = ERegTools.matches(reg, text);
			// trace(matches);
			// trace("before:"+text);
			for (m in matches) {
				// trace("m:"+m);
				if (text.startsWith(m))
				{
					// add correct one to resultsDict
					var arr = []; 
					if (resultsDict.exists(k)) 
						arr = resultsDict.get(k);
					
					arr.push(m);
					resultsDict.set(k,arr);
					// replace_one
					var i = text.indexOf(m);
					text = text.substr(i+m.length);
					// trace("text is now:"+text);
				}
			}
			// trace("after:"+text);
		}
		return resultsDict;
		
	}
	
	
		static function one_match(regex:EReg, currText:String):Dynamic //Null<String>
		  {
			var ok:Bool = regex.match(currText);
			var s = null;
			var x = null;
			if (ok) {
				x = regex.matchedPos();
				s = currText.substr(x.pos,x.len);
			}
			return {part:s,pos:x};
		}
	
	public static function matches(regex:EReg, text:String) 
	{
		// trace("in matches");
		var currText = text;
		var list = [];
		var from =  0;
		while (from <= text.length-1) 
		{
			// trace("step");
			// trace("current:"+currText);
			if (!regex.match(currText)) break;
			// trace("ok so far");
			var x = regex.matchedPos();
			// trace(x);
			if (x.len == 0) break;
			
			var s = currText.substr(x.pos,x.len);
			//if (!currText.startsWith(s)) break;
			// trace(x,s);
			list.push(s);
			var from = x.pos+x.len;
			currText = currText.substr(from);
			// trace(currText);
		}
		return list;
	}
	
 }


typedef Pair<A,B> =
{
	var first:A;
	var second:B;
}

